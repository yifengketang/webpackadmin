require('es6-promise').polyfill();
require('isomorphic-fetch');
function createItem(pic='',title='',desc=''){
    var str = '<div class="col-6 col-sm-6 col-md-4 col-lg-3 mb-4 item">'
       str +='<div class="box">'
         str += '<div class="img"><img src="'+pic+'" class="img-fluid" alt="" srcset=""></div>'
        str +='<div class="mt-2 title">'+title+'</div>'
        str += '<div class="desc">'+desc+'</div>';
         str +='</div>'
         str += '</div>';
         return str;
   }
 
   $("#loaddata").on('click',function(){
    //  var myRequest = new Request('./js/data.json');
    fetch('./js/data.json',{method:"GET"})
     .then(response => response.json())
     .then(data=>{
       console.log(data);
       var str="";
       data.list.forEach((value, index, array) => {
         str = createItem(value.pic,value.title,value.desc);
           $(".items").append(str);
         })
     });
   });