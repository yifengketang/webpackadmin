const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
// 生成htmlwebpackplugin配置
const createOptions = (options={},filename='index')=>{
    let tempoptions = {
        title:'网站首页',
        filename:filename + ".html",
        template:path.resolve(__dirname,`src/html/${filename}.html`),
        minify:{
            // collapseWhitespace:true,
            // removeComments:true,//移除注释
            // removeAttributQuotes:true,//移除属性的引导
            // minifyCSS:true,//CSS压缩
            // minifyJS:true//JS压缩
        },
        excludeChunks:[]
    }

    return Object.assign(tempoptions,options);
}
const CONFIG = {
    mode:"development",
    entry:{
        style:'./src/js/style.js',
        list:'./src/js/list.js'
    },
    output:{
        path:path.resolve(__dirname,"dist"),
        filename:'js/[name].js'
    },
    devServer:{
        // 设置站点根目录
        contentBase:"./dist",
        // 设置端口号
        port:3000,
        //设置自动打开浏览器
        open:true,
        // 开启热替换
        hot:true
    },
    module:{
        rules:[
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                  loader: 'babel-loader',
                  options: {
                    presets: ['@babel/preset-env']
                  }
                }
              },
            //处理CSS
            {
                test:/\.scss$/,
                use:[
                    // "style-loader",
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: '/',
                            // hmr: process.env.NODE_ENV === 'development'
                            hmr:true
                        },
                    },
                    "css-loader",
                    {
                        loader: 'postcss-loader',
                        options: {
                            ident: 'postcss',//设置惟一标识
                            plugins: loader => [
                                require("autoprefixer")({ browsers: ['cover 99.5%'] }) //添加前缀,根据浏览器版本
                            ]
                        }
                    },
                    "sass-loader"
                ]
            },
            {
                test:/\.tpl$/,
                loader:'html-loader'
            }
            ,
            //处理图片
            {
                test:/\.(jpe?g|png|gif)$/i,
                loader:"file-loader",
                options:{
                    limit: 10240,
                    name:'[hash:8].[ext]',
                    outputPath: 'images',
                    publicPath:"/images",
                    esModule:false
                }
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        // 文件拷贝
        new CopyPlugin([
            {from:'./src/js/data.json',to:'./js/data.json'}
        ]),
        //分离样式表
        new MiniCssExtractPlugin({
            filename: 'css/[name].css',
            ignoreOrder: false, // Enable to remove warnings about conflicting order
        }),
        new HtmlWebpackPlugin(createOptions()),
        new HtmlWebpackPlugin(createOptions({
            title:'课程列表'
        },'list')),
        new HtmlWebpackPlugin(createOptions({
            title:'关于我们',
            minify:{
                collapseWhitespace:true,
                removeComments:true,//移除注释
                removeAttributQuotes:true,//移除属性的引导
                minifyCSS:true,//CSS压缩
                minifyJS:true//JS压缩
            },
        },'about'))
      ]
}
module.exports = CONFIG;
